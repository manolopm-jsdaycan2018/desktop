# Graph-ic Desktop

Este es un proyecto de juguete que preparé para la JSDayCAN2018

La idea es tener un escritorio para integrar todas las demos. 
Lo usé para hacer pruebas de concepto locas, como carga dinámica y 
generación de los menús desde un json.

Para probarlo está en linea aquí: https://www.graph-ic.org/react-experiments/desktop/public

Para usarlo en local:
- npm run build