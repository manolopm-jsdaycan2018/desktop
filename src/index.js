import React from 'react'
import ReactDOM from 'react-dom'
import Desktop from './components/desktop.js'

ReactDOM.render(<Desktop />, document.getElementById('content'))
