'use strict'

import React from 'react'
import AppDispatcher from './appdispatcher.js'

export default class GroupIcon extends React.Component {
  constructor (props) {
    super(props)
    this.click = this.click.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.index !== nextProps.index) return true
    if (this.props.label !== nextProps.label) return true
    if (this.props.selected !== nextProps.selected) return true
    
    return false
  }
  

  click (evt) {
    AppDispatcher.dispatch({action: 'GroupIconClick',index: this.props.index})
  }
  
  render () {
    let style = {
      width: '32px',
      height: '32px',
      textAlign: 'center',
      color: '#a2a2a2',
      lineHeight: '32px',
      fontSize: '1.25em',
      display: 'block',
      alignSelf: 'flex-start',
      marginRight: '7px',
      cursor: 'pointer',
      userSelect: 'none'
    }

    if (this.props.selected === true) 
      style = Object.assign(
        style,
        {color: 'ghostwhite', borderBottom: '2px solid ghostwhite'}
      )

    return <div onClick={this.click} style={style} title={this.props.label.toUpperCase()}>{this.props.icon||this.props.label[0].toUpperCase()}</div>
  }
}
