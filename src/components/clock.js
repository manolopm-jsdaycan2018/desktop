'use strict'

import React from 'react'

class ClockDate extends React.PureComponent {
  render() {
    const dateStyle = {
      fontWeight: 'bold',
      fontSize: 'small',
      textAlign: 'center'
    }

    return <div style={dateStyle}>{this.props.date}</div>
  }
}

class ClockTime extends React.PureComponent {
  render() {
    const timeStyle = {
      fontWeight: 'bold',
      fontSize: 'smaller',
      textAlign: 'center'
    }

    return <div style={timeStyle}>{this.props.time}</div>
  }
}

export default class Clock extends React.Component {
  constructor (props) {
    super(props)
    const date = new Date()
    this.state = {time: date.toLocaleTimeString('es-ES'),
                  date: date.toLocaleDateString('es-ES', {weekday: 'long', day:'numeric', month: 'long'})}
    this.tick = this.tick.bind(this)
    setTimeout(this.tick, 1000)
  }

  tick() {
    const date = new Date()
    this.setState({time: date.toLocaleTimeString('es-ES'),
                   date: date.toLocaleDateString('es-ES', {weekday: 'long', day:'numeric', month: 'long'})},
                  () => setTimeout(this.tick, 1000))
  }
  
  render () {
    const style = {
      color: 'ghostwhite',
      marginLeft: '10px',
      marginRight: '10px',
      minWidth: '148px'
    }

    
    return  <div style={style}>
      <ClockTime time={this.state.time} />
      <ClockDate date={this.state.date} />
    </div>
  }
}
