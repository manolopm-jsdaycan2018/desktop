'use strict'

import React from 'react'

export default class MenuButton extends React.Component {
  constructor (props) {
    super(props)
    this.click = this.click.bind(this)
  }

  click (evt) {
    
  }

  shouldComponentUpdate(nextProps, nextState) {
    return false
  }

  
  render () {
    let style = {
      width: '32px',
      height: '32px',
      textAlign: 'center',
      color: 'ghostwhite',
      lineHeight: '32px',
      fontWeight: 'bold',
      fontSize: '1.25em',
      display: 'block',
      alignSelf: 'flex-start',
      marginRight: '7px',
      cursor: 'pointer'
    }
    return <div style={style}>{String.fromCharCode(8942)}</div>
  }
}
