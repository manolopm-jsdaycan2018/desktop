'use strict'

import React from 'react'
import WindowDesktop from './windowdesktop.js'
import GroupBar from './groupbar.js'
import IconBar from './iconbar.js'
import AppDispatcher from './appdispatcher.js'


export default class Desktop extends React.Component {
  constructor (props) { 
    super(props)
    this.process = this.process.bind(this)
    this.focus = this.focus.bind(this)
    this.refocus = this.refocus.bind(this)

    
    fetch('data.json', {method: 'GET',mode: 'cors'})
      .then(response => response.json().then(this.process))

    this.state = {
      groups: [],
      groupSelected: 0,
      windows: [],
      windowSelected: 0
    }
  }

  process (response) {
    this.setState(Object.assign(this.state, {groups:response.groups}))
  }

  focus(index) {
    let windows = this.state.windows
    let  zindex = windows[index].zindex
    
    return this.refocus(windows,zindex)
  }

  refocus(windows,sel) {
    return windows.map(window => {
      if (sel === window.zindex) return Object.assign(window, {zindex: windows.length-1})
      if (sel > window.zindex) return window
      if (sel < window.zindex) return Object.assign(window, {zindex: window.zindex-1})
    })
  }
  componentDidMount () {
    let windows = null
    let sel = -1
    let zindexsel = -1


    this.token =  AppDispatcher.register(event => {
      switch (event.action) {
      case 'MinimizeAll':
        windows = this.state.windows.map(window => Object.assign(window, {minimized: !window.minimized}))
        this.setState(Object.assign(this.state, {windowSelected: -1, windows: windows}))
        break
      case 'WindowMinimize':
        windows = this.state.windows
        windows[event.index].minimized = true
        sel = (event.index===this.state.windowSelected)? ((event.index!=windows.length-1)?windows.length-1:windows.length-2) :this.state.windowSelected
        this.setState(Object.assign(this.state, {windowSelected: sel, windows: windows}))
        break;
      case 'WindowMaximize':
        windows = this.focus(event.index)
        windows[event.index].maximized = !windows[event.index].maximized
        this.setState(Object.assign(this.state, {windowSelected: event.index, windows: windows}))
        break;        
      case 'WindowTaskClick':
        windows = this.focus(event.index)
        windows[event.index].minimized = false
        this.setState(Object.assign(this.state, {windowSelected: event.index, windows: windows}))        
        break
      case 'WindowClose':
        zindexsel = this.state.windows[event.index].zindex
        windows = this.state.windows.filter(window => window.index!=event.index)
        windows = windows.map((window,index)=>Object.assign(window,{index:index}))
        if (event.index===this.state.windowSelected) sel = windows.length - 1
        else if (event.index<this.state.windowSelected) sel = this.state.windowSelected - 1
        else sel = this.state.windowSelected
        this.setState(Object.assign(this.state, {windowSelected: sel, windows: this.refocus(windows, zindexsel)}))
        break
      case 'WindowMove':
        windows = this.focus(event.index)
        windows[event.index].x += event.x
        windows[event.index].y += event.y
        this.setState(Object.assign(this.state, {windowSelected: event.index, windows: windows}))
        break
      case 'GroupIconClick':
        this.setState(Object.assign(this.state, {groupSelected: event.index}))
        break
      case 'IconClick':
        let index
        if (this.state.windows.length>0)
          index = this.state.windows[this.state.windows.length-1].index+1
        else
          index = 0
        
        this.setState(
          Object.assign(this.state, {
            windowSelected: this.state.windows.length,
            windows: this.state.windows.concat(
              {
                name: event.name,
                x: 100 + 40 * this.state.windows.length,
                y: 100 + 40 * this.state.windows.length,
                w: 'maxContent',
                h: 'maxContent',
                minimized: false,
                maximized: false,
                index: index,
                zindex: this.state.windows.length
              }
            )})
        )
        break
      default:
        break
      }
    })
  }

  componentWillUnMount() {
    AppDispatcher.unregister(this.token)
  }
  
  render () {
    if (this.state.groups)
      return <div style={{
        maxWidth: '100%',
        width: '100%',
        height: '100%',
        display: 'grid',
        gridTemplateRows: '32px auto',
        gridTemplateColumns: '40px auto'
      }}>
      <WindowDesktop windows={this.state.windows} windowSelected={this.state.windowSelected} />
      <IconBar icons={this.state.groups[this.state.groupSelected] && this.state.groups[this.state.groupSelected].icons } />
      <GroupBar groups={this.state.groups} selected={this.state.groupSelected} windows={this.state.windows} windowSelected={this.state.windowSelected} />
      </div>
    else
      return <div>"NO DATA"</div>
  }
}

