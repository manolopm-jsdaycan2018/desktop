'use strict'

import React from 'react'
import AppDispatcher from './appdispatcher.js'

export default class WindowBar extends React.PureComponent {
  constructor (props) {
    super(props)
    this.click = this.click.bind(this)
    this.drag = this.drag.bind(this)
    this.clickMin = this.clickMin.bind(this)
    this.clickMax = this.clickMax.bind(this)
    this.clickClose = this.clickClose.bind(this)
  }

  click (evt) {
    AppDispatcher.dispatch({action: 'WindowTaskClick', index: this.props.data.index})
  }

  clickMin(evt) {
    AppDispatcher.dispatch({action: 'WindowMinimize', index: this.props.data.index})
    evt.stopPropagation()
  }

  clickMax(evt) {
    AppDispatcher.dispatch({action: 'WindowMaximize', index: this.props.data.index})
    evt.stopPropagation()
  }

  clickClose(evt) {
    AppDispatcher.dispatch({action: 'WindowClose', index: this.props.data.index})
    evt.stopPropagation()
  }
  
  drag (evt) {
    evt.dataTransfer.setData(
      'application/json',
      JSON.stringify({
        index: this.props.data.index,
        clientX: evt.clientX,
        clientY: evt.clientY
      }))
  }
  
  render () {
    let style = {
      width: '100%',
      height: '25px',
      minHeight: '25px',
      background: 'ghostwhite',
      borderRadius: '5px',
      borderBottomRightRadius: '0px',
      borderBottomLeftRadius: '0px',
      border: '0px',
      display:'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      userSelect: 'none',
      marginBottom: '1px'
    }

    let icoStyle = {
      width: '16px',
      height: '16px',
      background: '#404040',
      borderRadius: '4px',
      textAlign: 'center',
      marginLeft: '1px',
      border: '1px solid black',
      color: 'ghostwhite'
      
    }

    let icoCloseStyle = Object.assign({fontSize: '15px', marginRight: '4px'}, icoStyle)
    let icoMaxStyle = Object.assign({fontSize: '12px', fontWeight: 'bold' , marginRight: '1px'}, icoStyle)
    let icoMinStyle = Object.assign({fontSize: '16px', marginRight: '1px'}, icoStyle)
    let nameStyle = {display: 'flex', flex: '1', width: '16px', paddingLeft: '5px'}
    
    if (this.props.data.index === this.props.selected) {
      style = Object.assign(style, {background:'#d8d8dd'})
    }

    return <div draggable="true" onDragStart={this.drag} onClick={this.click} style={style}>
      <div style={nameStyle}>{this.props.data.name}</div>
      <div onClick={this.clickMin} style={icoMinStyle}>{String.fromCharCode(0x2012)}</div>
      <div onClick={this.clickMax} style={icoMaxStyle}>{String.fromCharCode(0x2610)}</div>
      <div onClick={this.clickClose} style={icoCloseStyle}>{String.fromCharCode(0xd7)}</div>
      </div>
  }
}
