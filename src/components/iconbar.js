'use strict'

import React from 'react'
import Icon from './icon.js'

export default class IconBar extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {compressed: true}
    this.clickExpand = this.clickExpand.bind(this)
  }

  clickExpand (evt) {
    this.setState({compressed:!this.state.compressed})
  }
  
  render () {

    return (
        <div style={{
          background: 'GhostWhite',
          paddingLeft: '4px',
          paddingRight: '4px',
          paddingTop: '10px',
          borderTop: 'none',
          borderRight: '1px solid #d8d8d8',
          gridRow: '2',
          width: 'max-content',
          gridTemplateColumns: 'max-content',
          zIndex: 999,
          userSelect: 'none'
          }}>
        <div style={{
          textAlign: 'right',
          cursor: 'pointer',
          fontSize: '2em',
          color: '#77b7e4'
        }}
      onClick={this.clickExpand}>{this.state.compressed?String.fromCharCode(8677):String.fromCharCode(8676)}</div>
        <div style={{marginTop: '10px'}}>
        {
          this.props.icons && this.props.icons.map(
            (icon, index) => 
              <Icon key={icon.description + index}
            icon={icon}
            index={index}
            compressed={this.state.compressed} />
          )
        }
        </div>
        </div>
    )
  }
}
