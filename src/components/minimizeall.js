'use strict'

import React from 'react'
import AppDispatcher from './appdispatcher.js'

export default class MinimizeAll extends React.Component {
  constructor (props) {
    super(props)
    this.click = this.click.bind(this)
  }

  click (evt) {
    AppDispatcher.dispatch({action: 'MinimizeAll'})
  }
  
  render () {
    let style = {
      width: '12px',
      height: '32px',
      textAlign: 'center',
      color: 'ghostwhite',
      borderLeft: '1px solid #7c7c7c',
      lineHeight: '32px',
      fontWeight: 'bold',
      fontSize: '1.25em',
      display: 'block',
      alignSelf: 'flex-end',
      cursor: 'pointer'
      }
    return <div onClick={this.click} title="Mostrar escritorio" style={style}></div>
  }
}
