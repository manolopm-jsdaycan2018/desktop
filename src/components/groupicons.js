'use strict'

import React from 'react'
import GroupIcon from './groupicon.js'

export default class GroupIcons extends React.Component {

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.selected !== nextProps.selected) return true
    if (this.props.groups.length !== nextProps.groups.length) return true
    
    return this.props.groups.map((element, index) => {
      if (element.name !== nextProps.groups[index].name) return true
    }).every(element => element === true)
  }

  
  render () {
    let style = {
      display: 'flex'
    }

    let groups = this.props.groups 
    
    if (groups.length <= 1)
      groups = []

    return <div style={style}>
      {
        groups.map((group, index) =>
                   <GroupIcon key={group.name} index={index} label={group.name}  selected={index === this.props.selected} /> 
        )
      }
    </div>
  }
}
