'use strict'

import React from 'react'
import Window from './window.js'
import AppDispatcher from './appdispatcher.js'

export default class WindowDesktop extends React.PureComponent {
  constructor (props) {
    super(props)
    this.drop = this.drop.bind(this)
    this.drag = this.drag.bind(this)
  }

  drop (evt) {
    evt.preventDefault();

    let data = JSON.parse(evt.dataTransfer.getData('application/json'))
    AppDispatcher.dispatch(
      {
        action: 'WindowMove',
        index: data.index,
        x: evt.clientX - data.clientX,
        y: evt.clientY - data.clientY
      })
  }

  drag (evt) {
    evt.preventDefault();
    return false;
  }

  render () {

    return (
        <div style={{
          width: '100%',
          height: '100%',
          flex: '1 0',
          border: '0px',
          display: 'flex',
          position: 'relative',
          background: 'white',
          gridRow: '2',
          gridColumn:  '2 / span 5'
        }}
      onDrop={this.drop}
      onDragOver={this.drag}
      onDragEnter={this.drag}>
        {
          this.props.windows.map(
            (window,i) => {
              let id = window.name+' '+window.index+'-'+i

              return (
                  <Window key={id} id={id} data={window} selected={this.props.windowSelected} content={window.name.replace(/ /g,'')}>
                  </Window>
              )
            }
          )
        }
        </div>
    )
  }
}

