'use strict'
import React from 'react'

export default class Wheel extends React.PureComponent {
  render () {
    return (
        <svg viewBox='0 0 36 36' style={this.props.style}>
        <g transform={`rotate(${this.props.angle-135||-135} 18 18)`}>
        <circle cx='18' cy='18' r='10' />
        <line strokeLinecap="round"
      x1="18" y1="18" x2="18" y2="10"
      stroke="grey" strokeWidth="1.2"/>
        
        <line strokeLinecap="round"
      x1="18" y1="18" x2="18" y2="10"
      stroke="white" strokeWidth="0.5"/>
        </g>
        <g>
        <circle cy='26.49' cx='9.51' r='0.7' />
        <circle cy='24' cx='7.61' r='0.4' />
        <circle cy='21.11' cx='6.41' r='0.4' />
        <circle cy='18' cx='6' r='0.6' />
        <circle cy='14.89' cx='6.41' r='0.4' />
        <circle cy='12' cx='7.61' r='0.4' />
        <circle cy='9.51' cx='9.51' r='0.5' />
        <circle cy='7.61' cx='12' r='0.4' />
        <circle cy='6.41' cx='14.89' r='0.4' />
        <circle cy='6' cx='18' r='0.6' />
        <circle cy='6.41' cx='21.11' r='0.4' />
        <circle cy='7.61' cx='24' r='0.4' />
        <circle cy='9.51' cx='26.49' r='0.5' />
        <circle cy='12' cx='28.39' r='0.4' />
        <circle cy='14.89' cx='29.59' r='0.4' />
        <circle cy='18' cx='30' r='0.6' />
        <circle cy='21.11' cx='29.59' r='0.4' />
        <circle cy='24' cx='28.39' r='0.4' />
        <circle cy='26.49' cx='26.49' r='0.7' />
        <text x='4' y='33' fontSize="5">MIN</text>
        <text x='22' y='33' fontSize="5">MAX</text>
        </g>
        </svg>)
  }
}
