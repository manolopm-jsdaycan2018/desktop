'use strict'
import React from 'react'

export default class Logger extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      error: '',
      inputs: '',
      outputs: '',
    }
    this.changeInput = this.changeInput.bind(this)
    this.sendMessage = this.sendMessage.bind(this)
    this.recordInput = this.recordInput.bind(this)
  }

  componentDidMount () {
    navigator.requestMIDIAccess().then(
      midi  => {
        let inputs = []
        let outputs = []
        midi.inputs.forEach(input => inputs.push({id:input.id, name:input.name}))
        midi.outputs.forEach(output => outputs.push({id:output.id, name:output.name}))
        this.setState({ midi:midi, error: '', inputs: inputs, outputs: outputs, data:'' })
      },
      error => this.setState({ midi:null, error: error, inputs: '', outputs: '', data:''})
    )
  }

  changeInput (evt) {
    this.state.midi.inputs.get(document.getElementById("in").value).onmidimessage = this.recordInput
  }

  sendMessage() {
    let output = this.state.midi.outputs.get(document.getElementById("out").value)
    let message = document.getElementById("message").value
    let note = document.getElementById("note").value
    let velocity = document.getElementById("velocity").value
    output.send([message, note, velocity])
  }

  recordInput (evt) {
    let date = new Date()
    console.log(evt)
    document.getElementById('notes').innerHTML =
      '<div>['+date.toISOString().replace('T',' ')+']'+evt.timeStamp+' '+evt.data[0] + ' ' + evt.data[1] + ' ' + evt.data[2] +'</div>'+
      document.getElementById('notes').innerHTML
  }
  
  render () {
    return (
        <div>
        <div>
        <label htmlFor="in"><u>I</u>nputs</label>
        <select accessKey="i" id="in" onChange={this.changeInput}>
        {this.state.inputs && this.state.inputs.map( input => <option key={input.id} value={input.id}>{input.name}</option>)}
        </select>
        <label htmlFor="out"><u>O</u>utputs</label>
        <select accessKey="o" id="out">
        {this.state.outputs && this.state.outputs.map( output => <option key={output.id} value={output.id}>{output.name}</option>)}
        </select>
        </div>
        <div>
        <label htmlFor="message"><u>M</u>essage</label>
        <input accessKey="m" id="message"></input>
        <label htmlFor="note"><u>N</u>ote</label>
        <input accessKey="n" id="note"></input>
        <label htmlFor="velocity"><u>V</u>elocity</label>
        <input accessKey="v" id="velocity"></input>
        <button accessKey="s" onClick={this.sendMessage}><u>S</u>END</button>
        </div>
        <div id="notes" style={{border:"1px solid black",height:"300px",overflow:"auto",paddingLeft:"10px"}}>
        {this.state.error}
        {this.state.data}
        </div>
        </div>
    )
  }
}
  
