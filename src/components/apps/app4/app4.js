import React from 'react'
import Button from './Button.js'

export default class Screen extends React.Component {
  constructor (props) {
    super(props)
    this.state = {error: '', inputs: '', outputs: ''}
  }

  componentDidMount () {
    navigator.requestMIDIAccess().then(
      midi => {
        let deviceIdIn = ''
        let deviceIdOut = ''

        midi.inputs.forEach(device => {
          if (device.name === 'Launchpad Pro MIDI 2') deviceIdIn = device.id
        })

        midi.outputs.forEach(device => {
          if (device.name === 'Launchpad Pro MIDI 2') deviceIdOut = device.id
        })

        this.setState({
          error: '',
          inputs: midi.inputs.get(deviceIdIn),
          outputs: midi.outputs.get(deviceIdOut)
        })

        for (let i = 0; i < 100; i++) {
          midi.outputs.get(deviceIdOut).send([0x80, i, 0x00])
        }
      },
      error =>
        this.setState({error: error, inputs: '', outputs: ''})
    )
  }

  render () {
    let buttons = []

    for (let i = 0; i < 10; i++) {
      for (let j = 0; j < 10; j++) {
        let round = false
        if ((i === 0 && j === 0) ||
            (i === 0 && j === 9) ||
            (i === 9 && j === 0) ||
            (i === 9 && j === 9)) continue

        if (i === 0 ||
            j === 0 ||
            i === 9 ||
            j === 9) round = true

        buttons.push(<Button key={i * 10 + j} row={i} column={j}
                     output={this.state.outputs}
                     input={this.state.inputs}
                     round={round}
                     />)
      }
    }

    return (
        <svg style={{
          width: '620px',
          height: '620px',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          background: 'black'
        }}>
      {buttons}
      </svg>
    )
  }
}
