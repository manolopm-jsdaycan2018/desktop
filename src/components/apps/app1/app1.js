'use strict'

import React from 'react'
//let Table
import Table from './table'
//import('./table').then(table=> Table=table.default)
import Data from './data'

export default class App1 extends React.PureComponent {
  render () {
    const innerStyle = {
      minWidth: 'max-content',
      background:'grey'
    }
    
    return (
        <Table innerStyle={innerStyle} data={Data}>
        </Table>
    )

  }
}

