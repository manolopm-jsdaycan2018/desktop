'use strict'
import React from 'react'
import TrackTop from './tracktop'
import TrackBottom from './trackbottom'
import TrackLeft from './trackleft'
import TrackRight from './trackright'
import TrackRoad from './trackroad'
import GoalTop from './goaltop'
import GoalBottom from './goalbottom'
import GoalLeft from './goalleft'
import GoalRight from './goalright'
import CornerUpLeft from './cornerupleft'
import CornerUpRight from './cornerupright'
import CornerDownLeft from './cornerdownleft'
import CornerDownRight from './cornerdownright'
import IntCornerUpLeft from './icornerupleft'
import IntCornerUpRight from './icornerupright'
import IntCornerDownLeft from './icornerdownleft'
import IntCornerDownRight from './icornerdownright'
import IntGreenDownRight from './igreendownright'
import IntGreenDownLeft from './igreendownleft'
import IntGreenUpRight from './igreenupright'
import IntGreenUpLeft from './igreenupleft'
import IntBrownDownRight from './ibrowndownright'
import IntBrownDownLeft from './ibrowndownleft'
import IntBrownUpRight from './ibrownupright'
import IntBrownUpLeft from './ibrownupleft'
import IntGreenDown from './igreendown'
import IntGreenUp from './igreenup'
import IntGreenRight from './igreenright'
import IntGreenLeft from './igreenleft'
import IntGreen1 from './igreen1'
import IntGreen2 from './igreen2'


export default class Track extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      track: [
        ['ibdr','igu','igu', 'igu', 'igu','igu','igu', 'igu', 'igu', 'ibdl','ig1','ig1','ig1', 'ig1', 'ig1', 'ig1' ],
        ['igl', 'cul','ht',  'ht',  'ht', 'ht', 'ht',  'ht',  'cur', 'igr', 'ig1','ig1','ig1', 'ig1', 'ig1', 'ig1' ],
        ['igl', 'vl', 'icdr','hb',  'hb', 'hb', 'hb',  'icdl','vr',  'igr', 'ig1','ig1','ig1', 'ig1', 'ig1', 'ig1' ],
        ['igl', 'vl', 'vr',  'igdr','igd','igd','igdl','vl',  'vr',  'igr', 'ig1','ig1','ig1', 'ig1', 'ig1', 'ig1' ],
        ['igl', 'vl', 'vr',  'igr', 'ig1','ig1','igl', 'vl',  'vr',  'igr', 'ig1','ig1','ig1', 'ig1', 'ig1', 'ig1' ],
        ['igl', 'vl', 'vr',  'igur','igu','igu','igul','vl',  'vr',  'igur','igu','igu','igu', 'igu', 'igu', 'ibdl'],
        ['igl', 'vl', 'icur','gt',  'ht', 'ht', 'ht',  'icul','icur','ht',  'ht', 'ht', 'ht',  'ht',  'cur', 'igr' ],
        ['igl', 'cdl','hb',  'gb',  'hb', 'hb', 'hb',  'icdl','icdr','hb',  'hb', 'hb', 'hb',  'icdl','vr',  'igr' ],
        ['ibur','igd','igd', 'igd', 'igd','igd','igdl','vl',  'vr',  'igdr','igd','igd','igdl','vl',  'vr',  'igr' ],
        ['ig1', 'ig1','ig1', 'ig1', 'ig1','ig1','igl', 'vl',  'vr',  'igr', 'ig1','ig1','igl', 'vl',  'vr',  'igr' ],
        ['ig1', 'ig1','ig1', 'ig1', 'ig1','ig1','igl', 'vl',  'vr',  'igur','igu','igu','igul','vl',  'vr',  'igr' ],
        ['ig1', 'ig1','ig1', 'ig1', 'ig1','ig1','igl', 'vl',  'icur','ht',  'ht', 'ht', 'ht',  'icul','vr',  'igr' ],
        ['ig1', 'ig1','ig1', 'ig1', 'ig1','ig1','igl', 'cdl', 'hb',  'hb',  'hb', 'hb', 'hb',  'hb',  'cdr', 'igr' ],
        ['ig1', 'ig1','ig1', 'ig1', 'ig1','ig1','ibur','igd', 'igd', 'igd', 'igd','igd','igd', 'igd', 'igd', 'ibul'],
      ]
    }
  }

  render () {
    return (
        <div style={{width: '1025px', height: '895px', position: 'relative', left: '0', top: '0'}}>
        {this.state.track.map((row, j) => {
          return row.map((tile, i) => {
            switch (tile) {
            case 'ht':
              return  <TrackTop key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'hb':
              return  <TrackBottom key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break              
            case 'vl':
              return  <TrackLeft key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'vr':
              return  <TrackRight key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'gt':
              return  <GoalTop key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'gb':
              return  <GoalBottom key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break              
            case 'gl':
              return  <GoalLeft key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'gr':
              return  <GoalRight key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64000px', height: '64000px', left: i*64, top: j*64}} />
                break
            case 'rd':
              return  <TrackRoad key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'cul':
              return  <CornerUpLeft key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64, background:'#bb8044'}} />
                break
            case 'cur':
              return  <CornerUpRight key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64, background:'#bb8044'}} />
                break
            case 'cdl':
              return  <CornerDownLeft key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64, background:'#bb8044'}} />
                break
            case 'cdr':
              return  <CornerDownRight key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64, background:'#bb8044'}} />
                break
            case 'icul':
              return  <IntCornerUpLeft key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'icur':
              return  <IntCornerUpRight key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'icdl':
              return  <IntCornerDownLeft key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'icdr':
              return  <IntCornerDownRight key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'igdr':
              return  <IntGreenDownRight key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'igdl':
              return  <IntGreenDownLeft key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break              
            case 'igul':
              return  <IntGreenUpLeft key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'igur':
              return  <IntGreenUpRight key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break              
            case 'ibdr':
              return  <IntBrownDownRight key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'ibdl':
              return  <IntBrownDownLeft key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break              
            case 'ibul':
              return  <IntBrownUpLeft key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'ibur':
              return  <IntBrownUpRight key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break              
            case 'igd':
              return  <IntGreenDown key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'igu':
              return  <IntGreenUp key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break              
            case 'igr':
              return  <IntGreenRight key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'igl':
              return  <IntGreenLeft key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break              
            case 'ig1':
              return  <IntGreen1 key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break
            case 'ig2':
              return  <IntGreen2 key={'tile'+i+'.'+j} style={{position: 'absolute', width: '64px', height: '64px', left: i*64, top: j*64}} />
                break                            
            }
          })
        })}
        </div>)
  }
}
