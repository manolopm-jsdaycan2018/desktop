'use strict'
import React from 'react'

export default class TrackRight extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <g transform="translate(-322 -100)">
        <path d="m384 100h2v16h-2v-16m2 32v16h-2v-16h2m-10-32h2v16h-2v-16m0 32h2v16h-2v-16" fill="#db6212"/>
        <path d="m376 132v32h-5v-64h5v32" fill="#9cc0c2"/>
        <path d="m384 100v16h-6v-16h6m-6 48v-16h6v16h-6" fill="#e86a17"/>
        <path d="m384 164h-6v-16h6v16m-6-48h6v16h-6v-16" fill="#fafafa"/>
        <path d="m378 116v16h-2v-16h2m6 0h2v16h-2v-16m2 32v16h-2v-16h2m-8 16h-2v-16h2v16" fill="#eee"/>
        <path d="m371 164h-49v-64h49v64" fill="#a6c9cb"/>
        </g>
        </svg>
    )
  }
}
