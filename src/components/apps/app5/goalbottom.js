'use strict'
import React from 'react'

export default class GoalBottom extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <g transform="translate(-1210 -322)">
        <path d="m1210 376h16v2h-16v-2m32 0h16v2h-16v-2m-32 8h16v2h-16v-2m32 2v-2h16v2h-16" fill="#eee"/>
        <path d="m1210 384v-6h16v6h-16m32 0v-6h16v6h-16" fill="#fafafa"/>
        <path d="m1210 376v-5h64v5h-64" fill="#9cc0c2"/>
        <path d="m1274 371h-64v-49h64v49" fill="#a6c9cb"/>
        <path d="m1226 384v-6h16v6h-16m32 0v-6h16v6h-16" fill="#e86a17"/>
        <path d="m1258 384h16v2h-16v-2m-32-8h16v2h-16v-2m32 0h16v2h-16v-2m-32 10v-2h16v2h-16" fill="#db6212"/>
        <path d="m1228 322h2v52h-2v-52m4 0h11v11h10v10h-10v11h10v11h-10v9h-11v-9h11v-11h-11v-11h11v-10h-11v-11m25 0v52h-2v-52h2" fill="#bddadb"/>
        </g>
        </svg>
    )
  }
}
