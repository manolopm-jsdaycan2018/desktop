'use strict'
import React from 'react'

export default class TrackBottom extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <g transform="translate(-248 -174)">
        <path d="m248 223v-49h64v49h-64" fill="#a6c9cb"/>
        <path d="m312 228v2h-16v-2h16m0 8v2h-16v-2h16m-32 2h-16v-2h16v2m0-10v2h-16v-2h16" fill="#db6212"/>
        <path d="m280 228h16v2h-16v-2m16 10h-16v-2h16v2m-32 0h-16v-2h16v2m-16-8v-2h16v2h-16" fill="#eee"/>
        <path d="m312 230v6h-16v-6h16m-32 0v6h-16v-6h16" fill="#e86a17"/>
        <path d="m280 230h16v6h-16v-6m-32 6v-6h16v6h-16" fill="#fafafa"/>
        <path d="m248 223h64v5h-64v-5" fill="#9cc0c2"/>
        </g>
        </svg>
    )
  }
}
