'use strict'
import React from 'react'

export default class GoalLeft extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <g transform="translate(-1062 -248)">
        <path d="m1070 248h2v16h-2v-16m-8 48v-16h2v16h-2m0-32v-16h2v16h-2m8 16h2v16h-2v-16" fill="#eee"/>
        <path d="m1070 280v16h-6v-16h6m-6-32h6v16h-6v-16" fill="#fafafa"/>
        <path d="m1072 312h-2v-16h2v16m-8 0h-2v-16h2v16m-2-32v-16h2v16h-2m8-16h2v16h-2v-16" fill="#db6212"/>
        <path d="m1070 264v16h-6v-16h6m0 48h-6v-16h6v16" fill="#e86a17"/>
        <path d="m1072 248h5v64h-5v-64" fill="#9cc0c2"/>
        <path d="m1077 248h49v64h-49v-64" fill="#a6c9cb"/>
        <path d="m1083 280h-9v-11h9v11h11v-11h11v11h10v-11h11v11h-11v10h-10v-10h-11v10h-11v-10m-9-13v-2h52v2h-52m0 27v-2h52v2h-52" fill="#bddadb"/>
        </g>
        </svg>
    )
  }
}
