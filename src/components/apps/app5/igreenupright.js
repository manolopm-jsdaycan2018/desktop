'use strict'
import React from 'react'

export default class IntGreenUpRight extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <g transform="translate(0 -148.1)">
        <path d="m32 148.1q-0.4 32.6 32 32v1.95q-34.45 0.7-34-33.95h2" fill="#b17940"/>
        <path d="m32 148.1h4q-0.35 28.55 28 28v4q-32.4 0.6-32-32" fill="#2fc06c"/>
        <path d="m30 148.1q-0.45 34.65 34 33.95v30.05h-64v-64h30m-12.85 34.65 0.8-7.05-7.1-0.8-0.85 7.05 7.15 0.8m6.6-3.7-4.35 0.7 0.7 4.35 4.35-0.7-0.7-4.35" fill="#bb8044"/>
        <path d="m23.75 179.05 0.7 4.35-4.35 0.7-0.7-4.35 4.35-0.7" fill="#c68a4e"/>
        <path d="m17.15 182.75-7.15-0.8 0.85-7.05 7.1 0.8-0.8 7.05" fill="#aa753e"/>
        <path d="m64 176.1q-28.35 0.55-28-28h28v28" fill="#27ae60"/>
        </g>
        </svg>
    )
  }
}
