'use strict'
import React from 'react'

export default class IntBrownDownLeft extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <defs>
        <linearGradient id="gradient5" gradientUnits="userSpaceOnUse" gradientTransform="matrix(0 -.0065308 .0065613 0 333.3 22.75)" x2="819.2" x1="-819.2">
        <stop stopColor="#27ae60" offset="0"/>
        <stop stopColor="#30c26d" offset="1"/>
        </linearGradient>        
        </defs>
        <g transform="translate(-296 -0.1)">
        <path d="m328.05 25v0.15l1.15 2.95h7.95l1.5-5.05-0.05-0.55v-0.05l-0.4-0.3q-0.3-0.15-0.55 0l-2.25 0.9-1.6-5.15-0.25-0.4-0.5-0.15-0.45 0.2-0.25 0.4-1.35 6.75-2.05-0.65-0.5 0.05-0.4 0.3v0.6m-32.05 3.1v-28h64v64h-28.1q1.65-36.15-35.9-36" fill="#27ae60"/>
        <path d="m328.05 25v-0.6l0.4-0.3 0.5-0.05 2.05 0.65 1.35-6.75 0.25-0.4 0.45-0.2 0.5 0.15 0.25 0.4 1.6 5.15 2.25-0.9q0.25-0.15 0.55 0l0.4 0.3v0.05l0.05 0.55-1.5 5.05h-7.95l-1.15-2.95v-0.15" fill="url(#gradient5)"/>
        <path d="m309.45 48.5-6.15-2.4-2.45 6.1 6.15 2.45 2.45-6.15m6.2 1.4-2.85-1.25-1.3 3 2.95 1.2 1.2-2.95m10.3 14.2h-29.95v-30q31.3-0.15 29.95 30" fill="#bb8044"/>
        <path d="m315.65 49.9-1.2 2.95-2.95-1.2 1.3-3 2.85 1.25" fill="#c68a4e"/>
        <path d="m309.45 48.5-2.45 6.15-6.15-2.45 2.45-6.1 6.15 2.4" fill="#aa753e"/>
        <path d="m296 32.1v-4q37.55-0.15 35.9 36h-3.9q1.45-32.15-32-32" fill="#2fc06c"/>
        <path d="m296 32.1q33.45-0.15 32 32h-2.05q1.35-30.15-29.95-30v-2" fill="#b17940"/>
        </g>
        </svg>
    )
  }
}
