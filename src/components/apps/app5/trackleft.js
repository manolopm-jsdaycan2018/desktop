'use strict'
import React from 'react'

export default class TrackLeft extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <g transform="translate(-174 -100)">
        <path d="m184 100h5v64h-5v-64" fill="#9cc0c2"/>
        <path d="m184 100v16h-2v-16h2m-10 48v-16h2v16h-2m0-32v-16h2v16h-2m10 16v16h-2v-16h2" fill="#eee"/>
        <path d="m176 100h6v16h-6v-16m0 32h6v16h-6v-16" fill="#fafafa"/>
        <path d="m176 132v-16h6v16h-6m6 32h-6v-16h6v16" fill="#e86a17"/>
        <path d="m182 164v-16h2v16h-2m-6 0h-2v-16h2v16m-2-32v-16h2v16h-2m10-16v16h-2v-16h2" fill="#db6212"/>
        <path d="m189 100h49v64h-49v-64" fill="#a6c9cb"/>
        </g>
        </svg>
    )
  }
}
