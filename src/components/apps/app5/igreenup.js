'use strict'
import React from 'react'

export default class IntGreenUp extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <defs>
        <linearGradient id="gradient17" gradientUnits="userSpaceOnUse" gradientTransform="matrix(0 -.0065308 .0065613 0 94 161.3)" x2="819.2" x1="-819.2">
        <stop stopColor="#27ae60" offset="0"/>
        <stop stopColor="#30c26d" offset="1"/>
        </linearGradient>        
        </defs>
        <g transform="translate(-74 -148.1)">
        <path d="m74 180.1q8-6.05 16 0t16 0 16 0 16 0v2q-8 6.05-16 0t-16 0-16 0q-2.5-1.9-4.95-2.6l-3.15-0.4q-3.95 0-7.9 3v-2" fill="#b17940"/>
        <path d="m74 180.1v-4q8-6.05 16 0t16 0 16 0 16 0v4q-8 6.05-16 0t-16 0-16 0-16 0" fill="#2fc06c"/>
        <path d="m138 182.1v30h-64v-30q3.95-3 7.9-3l3.15 0.4q2.45 0.7 4.95 2.6 8 6.05 16 0t16 0 16 0m-43.2 13.35-2.7 6.55 6.7 2.7 2.65-6.65-6.65-2.6" fill="#bb8044"/>
        <path d="m138 176.1q-8 6.05-16 0t-16 0-16 0-16 0v-28h64v28m-39.1-15.4q-0.3-0.15-0.55 0l-2.25 0.9-1.6-5.15-0.25-0.4-0.5-0.15-0.45 0.2q-0.2 0.15-0.25 0.4l-1.35 6.75-2.05-0.65-0.5 0.05-0.4 0.3v0.75l1.15 2.95h7.95l1.5-5.05-0.05-0.55v-0.05l-0.4-0.3" fill="#27ae60"/>
        <path d="m98.9 160.7 0.4 0.3v0.05l0.05 0.55-1.5 5.05h-7.95l-1.15-2.95v-0.75l0.4-0.3 0.5-0.05 2.05 0.65 1.35-6.75q0.05-0.25 0.25-0.4l0.45-0.2 0.5 0.15 0.25 0.4 1.6 5.15 2.25-0.9q0.25-0.15 0.55 0" fill="url(#gradient17)"/>
        <path d="m94.8 195.45 6.65 2.6-2.65 6.65-6.7-2.7 2.7-6.55" fill="#aa753e"/>
        </g>
        </svg>
    )
  }
}
