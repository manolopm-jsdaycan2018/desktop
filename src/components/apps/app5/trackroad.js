'use strict'
import React from 'react'

export default class TrackRoad extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <g transform="translate(-248 -100)">
        <path d="m248 164v-64h64v64h-64" fill="#a6c9cb"/>
        </g>
        </svg>
    )
  }
}
