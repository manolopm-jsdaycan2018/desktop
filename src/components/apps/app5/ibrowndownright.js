'use strict'
import React from 'react'

export default class IntBrownDownRight extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <defs>
        <linearGradient id="gradient3" gradientUnits="userSpaceOnUse" gradientTransform="matrix(0 -.0078735 .0080566 0 254.9 14)" x2="819.2" x1="-819.2">
        <stop stopColor="#27ae60" offset="0"/>
        <stop stopColor="#30c26d" offset="1"/>
        </linearGradient>
        <linearGradient id="gradient4" gradientUnits="userSpaceOnUse" gradientTransform="matrix(0 -.0065308 .0065613 0 233.95 37.5)" x2="819.2" x1="-819.2">
        <stop stopColor="#27ae60" offset="0"/>
        <stop stopColor="#30c26d" offset="1"/>
        </linearGradient>
        </defs>
        <g transform="translate(-222 0)">
        <path d="m252.25 7.45-0.4-0.05-0.4 0.25q-0.15 0.15-0.15 0.4l-0.65 5.75-2.75 0.55q-0.25 0.05-0.4 0.2l-0.1 0.15-0.1 0.3 0.05 0.4 0.05 0.05 2.4 5.15h9.8l2.65-1.75 0.2-0.25 0.1-0.2-0.1-0.55-0.45-0.35-2.5-0.7 2-5.15v-0.55l-0.05-0.1-0.35-0.3q-0.25-0.15-0.55 0l-4.65 1.65-3.25-4.65-0.4-0.25m-19 24.85-0.25 0.4-1.35 6.75-2.05-0.65-0.5 0.05-0.4 0.3v0.75l1.15 2.95h7.95l1.5-5.05-0.05-0.55v-0.05l-0.4-0.3q-0.3-0.15-0.55 0l-2.25 0.9-1.6-5.15-0.25-0.4-0.5-0.15-0.45 0.2m16.75 31.8h-28v-64h64v28.1q-36.15-1.65-36 35.9" fill="#27ae60"/>
        <path d="m252.25 7.45 0.4 0.25 3.25 4.65 4.65-1.65q0.3-0.15 0.55 0l0.35 0.3 0.05 0.1v0.55l-2 5.15 2.5 0.7 0.45 0.35 0.1 0.55-0.1 0.2-0.2 0.25-2.65 1.75h-9.8l-2.4-5.15-0.05-0.05-0.05-0.4 0.1-0.3 0.1-0.15q0.15-0.15 0.4-0.2l2.75-0.55 0.65-5.75q0-0.25 0.15-0.4l0.4-0.25 0.4 0.05" fill="url(#gradient3)"/>
        <path d="m233.25 32.3 0.45-0.2 0.5 0.15 0.25 0.4 1.6 5.15 2.25-0.9q0.25-0.15 0.55 0l0.4 0.3v0.05l0.05 0.55-1.5 5.05h-7.95l-1.15-2.95v-0.75l0.4-0.3 0.5-0.05 2.05 0.65 1.35-6.75 0.25-0.4" fill="url(#gradient4)"/>
        <path d="m286 32.1v2.05q-30.15-1.35-30 29.95h-2q-0.15-33.45 32-32" fill="#b17940"/>
        <path d="m286 32.1q-32.15-1.45-32 32h-4q-0.15-37.55 36-35.9v3.9" fill="#2fc06c"/>
        <path d="m286 34.15v29.95h-30q-0.15-31.3 30-29.95" fill="#bb8044"/>
        </g>
        </svg>
    )
  }
}
