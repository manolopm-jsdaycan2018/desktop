'use strict'
import React from 'react'

export default class IntBrownUpRight extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <defs>
        <linearGradient id="gradient8" gradientUnits="userSpaceOnUse" gradientTransform="matrix(0 -.0065308 .0065613 0 235.75 93.8)" x2="819.2" x1="-819.2">
        <stop stopColor="#27ae60" offset="0"/>
        <stop stopColor="#30c26d" offset="1"/>
        </linearGradient>
        <linearGradient id="gradient9" gradientUnits="userSpaceOnUse" gradientTransform="matrix(0 -.0065308 .0065613 0 260 126.25)" x2="819.2" x1="-819.2">
        <stop stopColor="#27ae60" offset="0"/>
        <stop stopColor="#30c26d" offset="1"/>
        </linearGradient>
        </defs>
        <g transform="translate(-222 -74.1)">
        <path d="m270.25 94.85 4.1-0.65-0.6-4.1-4.15 0.65 0.65 4.1m-0.6-8.4 7.45 0.8 0.8-7.5-7.4-0.8-0.85 7.5m-13.6-12.35h29.95v30q-31.3 0.15-29.95-30" fill="#bb8044"/>
        <path d="m269.65 86.45 0.85-7.5 7.4 0.8-0.8 7.5-7.45-0.8" fill="#c68a4e"/>
        <path d="m270.25 94.85-0.65-4.1 4.15-0.65 0.6 4.1-4.1 0.65" fill="#aa753e"/>
        <path d="m254 74.1h2.05q-1.35 30.15 29.95 30v2q-33.45 0.15-32-32" fill="#b17940"/>
        <path d="m254 74.1q-1.45 32.15 32 32v4q-37.55 0.15-35.9-36h3.9" fill="#2fc06c"/>
        <path d="m286 110.1v28h-64v-64h28.1q-1.65 36.15 35.9 36m-50.5-21.7-0.45 0.2q-0.2 0.15-0.25 0.4l-1.35 6.75-2.05-0.65-0.5 0.05-0.4 0.3v0.75l1.15 2.95h7.95l1.5-5.05-0.05-0.55v-0.05l-0.4-0.3q-0.3-0.15-0.55 0l-2.25 0.9-1.6-5.15-0.25-0.4-0.5-0.15m19.25 39.6v0.65l1.15 2.95h7.95l1.5-5.05-0.05-0.55v-0.05l-0.4-0.3q-0.3-0.15-0.55 0l-2.25 0.9-1.6-5.15-0.25-0.4-0.5-0.15-0.45 0.2q-0.2 0.15-0.25 0.4l-1.35 6.75-2.05-0.65-0.5 0.05q-0.25 0.1-0.4 0.3v0.1" fill="#27ae60"/>
        <path d="m235.5 88.4 0.5 0.15 0.25 0.4 1.6 5.15 2.25-0.9q0.25-0.15 0.55 0l0.4 0.3v0.05l0.05 0.55-1.5 5.05h-7.95l-1.15-2.95v-0.75l0.4-0.3 0.5-0.05 2.05 0.65 1.35-6.75q0.05-0.25 0.25-0.4l0.45-0.2" fill="url(#gradient8)"/>
        <path d="m254.75 128v-0.1q0.15-0.2 0.4-0.3l0.5-0.05 2.05 0.65 1.35-6.75q0.05-0.25 0.25-0.4l0.45-0.2 0.5 0.15 0.25 0.4 1.6 5.15 2.25-0.9q0.25-0.15 0.55 0l0.4 0.3v0.05l0.05 0.55-1.5 5.05h-7.95l-1.15-2.95v-0.65" fill="url(#gradient9)"/>
        </g>
        </svg>
    )
  }
}
