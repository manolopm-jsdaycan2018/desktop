'use strict'
import React from 'react'

export default class GoalTop extends React.PureComponent {

  render () {
    return (
        <svg style={this.props.style}>
        <g transform="translate(-1062 -321.95)">
        <path d="m1126 329.95h-16v-6h16v6m-32-6v6h-16v-6h16" fill="#fafafa"/>
        <path d="m1094 323.95h16v6h-16v-6m-16 0v6h-16v-6h16" fill="#e86a17"/>
        <path d="m1078 323.95v-2h16v2h-16m32 8v-2h16v2h-16m-16-2v2h-16v-2h16m16-6v-2h16v2h-16" fill="#eee"/>
        <path d="m1110 323.95h-16v-2h16v2m0 6v2h-16v-2h16m-32 0v2h-16v-2h16m-16-6v-2h16v2h-16" fill="#db6212"/>
        <path d="m1094 331.95h32v5h-64v-5h32" fill="#9cc0c2"/>
        <path d="m1126 336.95v49h-64v-49h64" fill="#a6c9cb"/>
        <path d="m1095 334h10v9h-10v11h10v11h-10v10h10v11h-10v-11h-11v-10h11v-11h-11v-11h11v-9m-15 0h2v52h-2v-52m27 0h2v52h-2v-52" fill="#bddadb"/>
        </g>
        </svg>
    )
  }
}
