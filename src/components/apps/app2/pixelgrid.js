'use strict'

import React from 'react'
import CtxMenu from './ctxmenu'
import AppDispatcher from '../../appdispatcher'

const cols = [
  'rgb(0, 0, 0)',       'rgb(33, 33, 33)',  'rgb(139, 139, 139)', 'rgb(251, 251, 251)', 'rgb(255, 100, 87)', 'rgb(255, 41, 0)',  'rgb(110, 10, 0)',  'rgb(34, 1, 0)',
  'rgb(255, 197, 114)', 'rgb(255, 108, 0)', 'rgb(110, 36, 0)',    'rgb(47, 26, 0)',     'rgb(251, 247, 37)', 'rgb(251, 247, 0)', 'rgb(104, 102, 0)', 'rgb(26, 25, 0)',
  'rgb(139, 246, 52)',  'rgb(63, 245, 0)',  'rgb(20, 101, 0)',    'rgb(16, 52, 0)',     '#30f534', '#00f500', '#006500', '#001900',
  '#30f657', '#00f500', '#006500', '#001900', '#30658f', '#00f546', '#006519', '#002012',
  '#002012', '#00f7a2', '#00663b', '#001912', '#3fcaff', '#00b6ff', '#004d63', '#00121a',
  '#5098ff', '#006cff', '#00266d', '#000421', '#5864ff', '#0433ff', '#01106d', '#000221',
  '#9665ff', '#6435ff', '#1b1277', '#0a0641', '#ff6aff', '#ff40ff', '#6e166d', '#220321',
  '#ff6694', '#ff2b62', '#6e0b21', '#290212', '#ff3400', '#ad4400', '#8c6200', '#4b7500',
  '#004500', '#006141', '#006490', '#0433ff', '#00525d', '#232adb', '#8b8b8b', '#282828',
  '#ff2900', '#c4f600', '#b7eb00', '#60f600', '#009500', '#00f68b', '#00b6ff', '#033dff',
  '#4333ff', '#8836ff', '#c33090', '#532900', '#ff5e00', '#91e200', '#71f600', '#00f500',
  '#00f500', '#47f672', '#00f8d2', '#6199ff', '#2d64d2', '#9590ef', '#dd3fff', '#ff2c6d',
  '#ff9100', '#c6ba00', '#95f600', '#956c00', '#473500', '#005b02', '#006147', '#121435',
  '#122c6d', '#7d4d19', '#bb1800', '#e96740', '#e57d00', '#ffe400', '#a4e200', '#6fbd00',
  '#21223b', '#e2f762', '#81f8c1', '#a7aaff', '#9a7bff', '#4d4d4d', '#868686', '#e2fbfb',
  '#b21700', '#420300', '#00d100', '#004b00', '#c6ba00', '#4d3b00', '#c36e00', '#591c00'
]
const colors = cols.map(a=>{if (a[0]==='r') return a; else return 'rgb('+parseInt(a.slice(1,3),16).toString(10)+', '+parseInt(a.slice(3,5),16).toString(10)+', '+parseInt(a.slice(5,7),16).toString(10)+')'})

export default class PixelGrid extends React.Component {
  constructor (props) {
    super (props)

    this.state = {
      columns: 'auto',
      rows: 'auto',
      matrix:[['']],
      menu:{visible: false, id: '', x: 0, y: 0}
    }
    this.getMidiMessage = this.getMidiMessage.bind(this)
    this.contextMenu = this.contextMenu.bind(this)
  }

  getMidiMessage (midiMessage) {


    switch (midiMessage.data[0]) {
    case 176:
      switch (midiMessage.data[2]) {
      case 127:
        switch (midiMessage.data[1]) {
        case 10:
          this.setState(Object.assign(this.state, {columns:this.state.columns+' auto', matrix: this.state.matrix.map(row => row.concat(''))}))
          break
        case 20:
          this.setState(Object.assign(this.state, {rows:this.state.rows+' auto', matrix: this.state.matrix.concat([new Array(this.state.columns.split(' ').length).fill('')])}))
          break
        }
        break
      }
      break
    }

  }

  componentDidMount () {

    this.token =  AppDispatcher.register(event => {
      switch (event.action){
      case 'ColorSelect':
        let matrix = this.state.matrix
        let split = event.element.split('-')
        matrix[split[1]][split[2]] = event.color

        this.setState(Object.assign(
          this.state,
          {menu: {
            visible: false,
            id: '',
            x: 0,
            y: 0},
           matrix: matrix
          }))
        
        break;
      }
    })
    
    navigator.requestMIDIAccess().then(
      midi => {
        let deviceIdIn = ''
        let deviceIdOut = ''
        midi.inputs.forEach(device => {
          if (device.name === 'Launchpad Pro MIDI 2') deviceIdIn = device.id
        })

        midi.outputs.forEach(device => {
          if (device.name === 'Launchpad Pro MIDI 2') deviceIdOut = device.id
        })

        this.setState(Object.assign(
          this.state,
          {
            input: midi.inputs.get(deviceIdIn),
            output: midi.outputs.get(deviceIdOut)
          }))

        if (midi.inputs.get(deviceIdIn))
          midi.inputs.get(deviceIdIn).onmidimessage = this.getMidiMessage
        
        for (let i = 0; i < 100; i++) {
          if (midi.outputs.get(deviceIdOut))
            midi.outputs.get(deviceIdOut).send([0x80, i, 0])
        }
      },
      error => {
        console.log("MIDI ERROR:", error)
        this.setState(Object.assign(this.state, {input: null, output: null}))
      }
    )
    
  }

  componentWillUnMount() {
    AppDispatcher.unregister(this.token)
  }

  contextMenu (event) {
    event.preventDefault()
    this.setState(Object.assign(
      this.state,
      {menu: {
        visible: true,
        id:event.target.id,
        x:event.clientX,
        y:event.clientY}}))

  }
  
  render() {
    let style = {
      display: 'grid',
      gridTemplateColumns: this.state.columns,
      gridTemplateRows: this.state.rows,
      height: '100%',
      minWidth:'400px'
      
    }
    
    let divStyle = {
      border: '1px solid black'
    }
    
    let count =
        this.state.columns.split(' ').length *
        this.state.rows.split(' ').length

    let menu
    
    if (this.state.menu.visible===true)
      menu = <CtxMenu id={this.state.menu.id} x={this.state.menu.x} y={this.state.menu.y} ></CtxMenu>
      
    
    let col = new Array(this.state.rows.split(' ').length).fill(0).map((a,row) => {
      return new Array(this.state.columns.split(' ').length).fill(0).map((b,column) => {
        let idx = colors.indexOf(this.state.matrix[row][column])
        if (idx === -1) idx = 0

        
        let nCols = (8/this.state.columns.split(' ').length)
        let nRows = (8/this.state.rows.split(' ').length)
        
        for (let i=0;i<nCols;i++) {
          for (let j=0;j<nRows;j++) {
            if (this.state.output) this.state.output.send([144,81+((column*nCols)+i)-(10*((row*nRows)+j)),idx])
          }
        }
        
        return <div onContextMenu={this.contextMenu} id={"divGrid-"+row+'-'+column} key={"divGrid-"+row+'-'+column} style={Object.assign({background:this.state.matrix[row][column]},divStyle)}>{"divGrid-"+row+'-'+column}{menu}</div>
      })
    })
                                                                       
    return <div style={style}>
      {col}
    </div>
  }
}
