'use strict'

import React from 'react'
import AppDispatcher from '../../appdispatcher'


const colors = [
  '#000000', '#212121', '#8b8b8b', '#fbfbfb', '#ff6457', '#ff2900', '#6e0a00', '#220100',
  '#ffc572', '#ff6c00', '#6e2400', '#2f1a00', '#fbf725', '#fbf700', '#686600', '#1a1900',
  '#8bf634', '#3ff500', '#146500', '#103400', '#30f534', '#00f500', '#006500', '#001900',
  '#30f657', '#00f500', '#006500', '#001900', '#30658f', '#00f546', '#006519', '#002012',
  '#002012', '#00f7a2', '#00663b', '#001912', '#3fcaff', '#00b6ff', '#004d63', '#00121a',
  '#5098ff', '#006cff', '#00266d', '#000421', '#5864ff', '#0433ff', '#01106d', '#000221',
  '#9665ff', '#6435ff', '#1b1277', '#0a0641', '#ff6aff', '#ff40ff', '#6e166d', '#220321',
  '#ff6694', '#ff2b62', '#6e0b21', '#290212', '#ff3400', '#ad4400', '#8c6200', '#4b7500',
  '#004500', '#006141', '#006490', '#0433ff', '#00525d', '#232adb', '#8b8b8b', '#282828',
  '#ff2900', '#c4f600', '#b7eb00', '#60f600', '#009500', '#00f68b', '#00b6ff', '#033dff',
  '#4333ff', '#8836ff', '#c33090', '#532900', '#ff5e00', '#91e200', '#71f600', '#00f500',
  '#00f500', '#47f672', '#00f8d2', '#6199ff', '#2d64d2', '#9590ef', '#dd3fff', '#ff2c6d',
  '#ff9100', '#c6ba00', '#95f600', '#956c00', '#473500', '#005b02', '#006147', '#121435',
  '#122c6d', '#7d4d19', '#bb1800', '#e96740', '#e57d00', '#ffe400', '#a4e200', '#6fbd00',
  '#21223b', '#e2f762', '#81f8c1', '#a7aaff', '#9a7bff', '#4d4d4d', '#868686', '#e2fbfb',
  '#b21700', '#420300', '#00d100', '#004b00', '#c6ba00', '#4d3b00', '#c36e00', '#591c00'
]



export default class CtxMenu extends React.Component {
  constructor(props) {
    super(props)
    this.click = this.click.bind(this)
  }

  click(event) {
    AppDispatcher.dispatch(
      {action: 'ColorSelect',
       element: event.target.id.split('#')[0],
       color: event.target.style.background,
       colorIndex: colors.indexOf(event.target.style.background)
      })
  }
  
  render(){
    let style = {position: 'fixed',
                 left: this.props.x,
                 top: this.props.y,
                 display: 'grid',
                 gridTemplateColumns: 'auto auto auto auto auto auto auto auto',
                 gridTemplateRows: 'auto auto auto auto auto auto auto auto auto auto auto auto auto auto auto auto',
                 minWidth: '100px',
                 minHeight: '100px',
                 borderRadius: '1px',
                 padding: '1px',
                 boxShadow: "0 0 10px #888",
                 background: '#eee'
                }

    let boxStyle = {width: '40px', height: '20px', margin:'2px', border: '1px solid black'}
    let boxes = colors.map((element,i)=>{
      return <div id={i+element} id={this.props.id+'#ctxmenu#'+colors[i]} onClick={this.click}
      key={i+element} style={Object.assign({background:colors[i]},boxStyle)}></div>
    })

    return <div style={style} id={this.props.id+'#ctxmenu'}>
      {boxes}  
      </div>
  }
}
