'use strict'

import React from 'react'
import AppDispatcher from './appdispatcher.js'
import WindowBar from './windowbar.js'
import WindowContent from './windowcontent.js'
import Loadable from 'react-loadable'

export default class Window extends React.PureComponent {
  constructor (props) {
    super(props)
    this.click = this.click.bind(this)
    this.drag = this.drag.bind(this)
    this.dragging = this.dragging.bind(this)
    this.state = {}
    const name = this.props.content.toLowerCase()
    this.App = Loadable({
      loader: () => import(`./apps/${name}/${name}.js`),
      loading: () => <div>Loading...</div>
    })

  }

  click (evt) {
    AppDispatcher.dispatch({action: 'WindowTaskClick', index: this.props.data.index})
  }

  dragging (evt) {
    //    this.setState(Object.assign(this.state,{w:evt.clientX-this.state.x}))
  }

  drag (evt) {
//    console.log(this.state)
//    console.log(this.state.x+50,evt.clientX,this.state.x+this.state.w-20)

    if (evt.clientX>this.props.data.x+this.props.data.w-20) console.log('right border')
    if (evt.clientX<this.props.data.x+50) console.log('left border')
    
    evt.dataTransfer.setData(
      'application/json',
      JSON.stringify({
        index: this.props.data.index,
        clientX: evt.clientX,
        clientY: evt.clientY
      }))

  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return nextProps.data
  }
  
  render () {
    const state = this.props.data
    let style = {
      width: state.w,
      height: state.h,
      left: state.x,
      top: state.y,
      borderRadious: '20%',
      background: '#bbbbbb',
      border: '1px solid ghostwhite',
      position: 'absolute',
      display: 'flex',
      flexDirection: 'column',
      borderRadius: '5px',
      border: '0px',
      padding: '1px',
      zIndex: state.zindex,
      boxShadow: '0 4px 6px 0 hsla(0, 0%, 0%, 0.2)',
    }
    
    if (state.index === this.props.selected)
      style = Object.assign(style, {background: 'black'})

    if (this.props.data.minimized === true)
      style = Object.assign(style, {display: 'none'})

    if (this.props.data.maximized === true)
      style = Object.assign(style, {border: '0px', width: '100%', height: '100%', left: 0, top: 0, padding:'0px'})
    
    return <div draggable="false" id={this.props.id} onDragStart={this.drag} onDrag={this.dragging} onClick={this.click} style={style}>
      <WindowBar data={state} />
      <WindowContent>{React.createElement(this.App,{key:'c '+this.props.id},null)}</WindowContent>
      </div>
  }
}


