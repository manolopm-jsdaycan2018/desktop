'use strict'

import React from 'react'
import GroupIcons from './groupicons.js'
import TaskBar from './taskbar.js'
import MenuButton from './menubutton.js'
import MinimizeAll from './minimizeall.js'
import Clock from './clock.js'

export default class IconBar extends React.PureComponent {
  render () {
    return (
        <div style={{minWidth: '100%',
                     maxWidth: '100%',
                     width: '100%',
                     background: 'black',
                     borderBottom: '1 px solid aliceblue',
                     boxShadow: 'rgb(144,144,144) -2px 3px 24px 0px',
                     flex: '0 1',
                     display: 'flex',
                     maxHeight: '32px',
                     gridColumn: '1 / span 6',
                     flexDirection: 'row',
                     zIndex: 999
                    }}>
        <MenuButton />
        <GroupIcons groups={this.props.groups} selected={this.props.selected}  />
        <TaskBar windows={this.props.windows} selected={this.props.windowSelected} />
        <Clock />
        <MinimizeAll />
        </div>
    )
  }
}
