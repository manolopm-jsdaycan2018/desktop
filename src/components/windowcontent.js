'use strict'

import React from 'react'

export default class WindowContent extends React.PureComponent {
  render () {
    const style = {
      height: '100%',
      background: '#eeeeee',
      borderBottomLeftRadius: '4px',
      borderBottomRightRadius: '4px',
      padding: '5px',
      overflow: 'hidden'
    }
    return <div style={style}>
      {this.props.children}
      </div>
  }
}
