'use strict'

import React from 'react'
import AppDispatcher from './appdispatcher.js'

export default class WindowTask extends React.PureComponent {
  constructor (props) {
    super(props)
    this.click = this.click.bind(this)
  }

  click (evt) {
    AppDispatcher.dispatch({action: 'WindowTaskClick', index: this.props.data.index})
  }

  render () {

    let windowStyle = {
      fontSize: 'smaller',
      background: 'black',
      border: '1px solid #a2a2a2',
      color: 'ghostwhite',
      flex: '1',
      display: 'flex',
      borderRadius: '5px',
      justifyContent: 'center',
      alignItems: 'center',
      cursor: 'pointer',
      userSelect: 'none'
    }

    if (this.props.data.index === this.props.selected)
      windowStyle = Object.assign( windowStyle, {
        background:'#646464',
        color: 'white',
        borderBottom: '1px solid ghostwhite'
      })

    return <div onClick={this.click} style={windowStyle}>{this.props.data.name}</div>
  }
}
