'use strict'

import React from 'react'
import AppDispatcher from './appdispatcher.js'

class IconAvatar extends React.PureComponent {
  render() {
    return <div style={this.props.style} title={this.props.content}>{this.props.content[0]}</div>
  }
}

export default class Icon extends React.PureComponent {
  constructor (props) {
    super(props)
    this.click = this.click.bind(this)
  }

  click (evt) {
    AppDispatcher.dispatch({action: 'IconClick', name: this.props.icon.name})
  }

  render () {
    const palette = [
      '#f98bae', '#e57979', '#ecd074', '#6fb1e4',
      '#fba76f', '#80d066', '#cc90e2', '#ff5722',
      '#009688', '#2196f3', '#9e9e9e', '#795544'
    ]

    const state = {
      style: {
        background: palette[this.props.index%12],
        marginTop: '4px',
        marginBottom: '4px',
        borderRadius: '50%',
        height: '30px',
        width: '30px',
        color: 'white',
        textAlign: 'center',
        lineHeight: '30px',
        fontSize: '18px',
        display: 'block',
        userSelect: 'none',
        gridColumn: '1'
      },
      outStyle: {
        display: 'grid',
        cursor: 'pointer',
        userSelect: 'none'
      },
      inStyle: {
        paddingLeft: '10px',
        paddingRight: '10px',
        lineHeight: '40px',
        height: '30px',
        fontWeight: 'bold',
        fontSize: '0.9em',
        userSelect: 'none',
        gridColumn: '2'
      }
    }



    let content = this.props.icon.description
    content = content[0].toUpperCase() + content.slice(1)
    
    return <div onClick={this.click} style={state.outStyle}>
      <IconAvatar style={state.style} content={content} />
      {!this.props.compressed && <div style={this.props.compressed?{}:state.inStyle} title={content}>{content}</div>}
      </div>
  }
}
