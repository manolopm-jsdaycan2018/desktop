'use strict'

import React from 'react'
import WindowTask from './windowtask.js'

export default class TaskBar extends React.PureComponent {
  render () {
    let style = {
      flex: '1',
      display: 'flex',
      marginTop: '3px',
      marginBottom: '3px',
      borderRadius: '6px',
      background: '#a2a2a2',
      marginRight: '10px',
      marginLeft: '10px',
      paddingLeft: '5px',
      paddingRight: '5px',
      paddingTop: '1px',
      paddingBottom: '1px'
    }
    
    return <div style={style}>
      {
        this.props.windows.map(
          window => <WindowTask key={'task'+window.name+' '+window.index+' '+Date.now()} data={window} selected={this.props.selected}/>
        )
      }
      </div>
  }
}
